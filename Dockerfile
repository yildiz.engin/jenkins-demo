FROM node:lts-alpine

RUN apk add curl python3 py3-pip git openssh-client && pip3 install awscli

WORKDIR /app

COPY . .


